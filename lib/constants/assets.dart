class Assets {
  Assets._();
  
  /// Assets for fontsCairoCairoRegular
  /// assets/fonts/Cairo/Cairo-Regular.ttf
  static const String fontsCairoCairoRegular = "assets/fonts/Cairo/Cairo-Regular.ttf";

  /// Assets for fontsDMSansDMSansRegular
  /// assets/fonts/DM_Sans/DMSans-Regular.ttf
  static const String fontsDMSansDMSansRegular = "assets/fonts/DM_Sans/DMSans-Regular.ttf";

  /// Assets for fontsPoppinsPoppinsBold
  /// assets/fonts/Poppins/Poppins-Bold.ttf
  static const String fontsPoppinsPoppinsBold = "assets/fonts/Poppins/Poppins-Bold.ttf";

  /// Assets for fontsPoppinsPoppinsMedium
  /// assets/fonts/Poppins/Poppins-Medium.ttf
  static const String fontsPoppinsPoppinsMedium = "assets/fonts/Poppins/Poppins-Medium.ttf";

  /// Assets for fontsPoppinsPoppinsRegular
  /// assets/fonts/Poppins/Poppins-Regular.ttf
  static const String fontsPoppinsPoppinsRegular = "assets/fonts/Poppins/Poppins-Regular.ttf";

  /// Assets for fontsPoppinsPoppinsSemiBold
  /// assets/fonts/Poppins/Poppins-SemiBold.ttf
  static const String fontsPoppinsPoppinsSemiBold = "assets/fonts/Poppins/Poppins-SemiBold.ttf";

  /// Assets for iconsActivity
  /// assets/icons/Activity.svg
  static const String iconsActivity = "assets/icons/Activity.svg";

  /// Assets for iconsAttachment
  /// assets/icons/Attachment.svg
  static const String iconsAttachment = "assets/icons/Attachment.svg";

  /// Assets for iconsBell
  /// assets/icons/bell.svg
  static const String iconsBell = "assets/icons/bell.svg";

  /// Assets for iconsBell1
  /// assets/icons/bell-1.svg
  static const String iconsBell1 = "assets/icons/bell-1.svg";

  /// Assets for iconsCalendar
  /// assets/icons/calendar.svg
  static const String iconsCalendar = "assets/icons/calendar.svg";

  /// Assets for iconsCall
  /// assets/icons/Call.svg
  static const String iconsCall = "assets/icons/Call.svg";

  /// Assets for iconsChat
  /// assets/icons/chat.svg
  static const String iconsChat = "assets/icons/chat.svg";

  /// Assets for iconsClock
  /// assets/icons/clock.svg
  static const String iconsClock = "assets/icons/clock.svg";

  /// Assets for iconsDots
  /// assets/icons/Dots.svg
  static const String iconsDots = "assets/icons/Dots.svg";

  /// Assets for iconsDropdown
  /// assets/icons/dropdown.svg
  static const String iconsDropdown = "assets/icons/dropdown.svg";

  /// Assets for iconsEmail
  /// assets/icons/Email.svg
  static const String iconsEmail = "assets/icons/Email.svg";

  /// Assets for iconsFalse
  /// assets/icons/false.png
  static const String iconsFalse = "assets/icons/false.png";

  /// Assets for iconsFamily
  /// assets/icons/family.svg
  static const String iconsFamily = "assets/icons/family.svg";

  /// Assets for iconsFinance
  /// assets/icons/finance.svg
  static const String iconsFinance = "assets/icons/finance.svg";

  /// Assets for iconsFood
  /// assets/icons/food.svg
  static const String iconsFood = "assets/icons/food.svg";

  /// Assets for iconsGear
  /// assets/icons/gear.svg
  static const String iconsGear = "assets/icons/gear.svg";

  /// Assets for iconsHome
  /// assets/icons/home.svg
  static const String iconsHome = "assets/icons/home.svg";

  /// Assets for iconsLocation
  /// assets/icons/Location.svg
  static const String iconsLocation = "assets/icons/Location.svg";

  /// Assets for iconsMarks
  /// assets/icons/marks.png
  static const String iconsMarks = "assets/icons/marks.png";

  /// Assets for iconsMarks2
  /// assets/icons/marks2.png
  static const String iconsMarks2 = "assets/icons/marks2.png";

  /// Assets for iconsPrint
  /// assets/icons/print.svg
  static const String iconsPrint = "assets/icons/print.svg";

  /// Assets for iconsQuote
  /// assets/icons/Quote.svg
  static const String iconsQuote = "assets/icons/Quote.svg";

  /// Assets for iconsSearch
  /// assets/icons/Search.svg
  static const String iconsSearch = "assets/icons/Search.svg";

  /// Assets for iconsSell
  /// assets/icons/sell.png
  static const String iconsSell = "assets/icons/sell.png";

  /// Assets for iconsSent
  /// assets/icons/sent.svg
  static const String iconsSent = "assets/icons/sent.svg";

  /// Assets for iconsStudent
  /// assets/icons/student.svg
  static const String iconsStudent = "assets/icons/student.svg";

  /// Assets for iconsStudent2
  /// assets/icons/student2.png
  static const String iconsStudent2 = "assets/icons/student2.png";

  /// Assets for iconsStudentPng
  /// assets/icons/studentPng.png
  static const String iconsStudentPng = "assets/icons/studentPng.png";

  /// Assets for iconsTeacher
  /// assets/icons/teacher.svg
  static const String iconsTeacher = "assets/icons/teacher.svg";

  /// Assets for iconsTrending
  /// assets/icons/trending.svg
  static const String iconsTrending = "assets/icons/trending.svg";

  /// Assets for iconsTrue
  /// assets/icons/true.png
  static const String iconsTrue = "assets/icons/true.png";

  /// Assets for iconsUser
  /// assets/icons/User.svg
  static const String iconsUser = "assets/icons/User.svg";

  /// Assets for iconsVideo
  /// assets/icons/Video.svg
  static const String iconsVideo = "assets/icons/Video.svg";

  /// Assets for imagesLogo
  /// assets/images/logo.png
  static const String imagesLogo = "assets/images/logo.png";

  /// Assets for imagesMasking
  /// assets/images/Masking.png
  static const String imagesMasking = "assets/images/Masking.png";

  /// Assets for imagesMaskingStudent
  /// assets/images/MaskingStudent.png
  static const String imagesMaskingStudent = "assets/images/MaskingStudent.png";

  /// Assets for imagesMaskingTeacher
  /// assets/images/MaskingTeacher.png
  static const String imagesMaskingTeacher = "assets/images/MaskingTeacher.png";
}

