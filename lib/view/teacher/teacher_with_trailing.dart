import 'package:flutter/material.dart';
import 'package:sama/view/teacher/teacher_view.dart';
import 'package:sama/utils/header_with_search.dart';
import 'package:sama/view/trailing_teacher/trailing_teacher.dart';

class TeachersWithTrailing extends StatelessWidget {
  const TeachersWithTrailing({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: Padding(
            padding: EdgeInsets.all(40),
            child: Column(
              children: [
                HeaderWithSearch(title: "Teacher Details"),
                SizedBox(height: 28),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(flex: 1180, child: Teachers()),
                    Expanded(flex: 395, child: TrailingTeacher()),
                  ],
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
