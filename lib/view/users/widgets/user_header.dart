import 'package:flutter/material.dart';
import 'package:sama/constants/app_font_style.dart';
import 'package:sama/utils/custom_search.dart';

class UsersHeader extends StatelessWidget {
  const UsersHeader({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          "User Dashboard",
          style: AppFontStyle.styleBold36(context),
        ),
        SizedBox(
          width: 350 * getScaleFactor(context),
          child: const CustomSearch(),
        )
      ],
    );
  }
}
